*** Settings ***
Library    Browser
Library    String

*** Variables ***
${url}    http://localhost:8080
${h_login_link}    //a[@title="Identifiez-vous"]
${h_logout_link}    //a[contains(@href, "?mylogout=") and @class="logout hidden-sm-down"]
${h_welcome_message}    //a[@title="Voir mon compte client"]/span  
${log_login_button}    //*[@id="submit-login"]
${log_email_field}    //*[@id="field-email"]
${log_password_field}    //*[@id="field-password"]


${mail}    harry.potter@hogwarts.fr
${pass}    hedwige1234
${firstName}    Harry
${lastName}    Potter


*** Test Cases ***
Test with datasets parameters
    [Setup]    Open Browser    ${url}
    Given I am on the login Page
    When I login with email "${mail}" and password "${pass}"
    Then I am logged with first "${firstName}" and last "${lastName}"
    [Teardown]    Close Browser


Test with injected parameters
    [Setup]    Open Browser    ${url}
    Given I am on the login Page
    When I login with email "harry.potter@hogwarts.fr" and password "hedwige1234"
    Then I am logged with first "Harry" and last "Potter"
    [Teardown]    Close Browser

*** Keywords ***
I am on the login Page
    Click    ${h_login_link}

I login with email "${email}" and password "${password}"
    Log To Console    ******Login :******
    Log To Console    ${mail}--${password}
    Type Text    ${log_email_field}    ${email}
    Type Text    ${log_password_field}    ${password}
    Click    ${log_login_button}

I am logged with first "${first}" and last "${last}"
    Log To Console    ******Logged :******
    Log To Console    ${first}--${last}
    Get Text    ${h_logout_link}    *=    Déconnexion
    Get Text    ${h_welcome_message}    ==    ${first} ${last}
