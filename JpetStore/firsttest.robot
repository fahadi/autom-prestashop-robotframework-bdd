*** Settings ***
Library    Browser

*** Variables ***
${url}    https://petstore.octoperf.com/actions/Account.action?signonForm=
${username}    j2ee
${password}    j2ee
${username_field}    //*[@name="username"]
${password_field}    //*[@name="password"]
${login_button}    //*[@name="signon"]
${welcome_label}    //*[@id="WelcomeContent"]

*** Test Cases ***
The Test
    I go to JpetStore and I login
    I am logged in jpetsore


*** Keywords ***
I go to JpetStore and I login
    New Browser    browser=chromium   headless=True
    New Page    url=${url}
    Set Viewport Size    1900   1000 

    Fill Text    ${username_field}    ${username}
    Fill Text    ${password_field}    ${password}

    Click    ${login_button}    force=True

I am logged in jpetsore
    Get Text    ${welcome_label}    ==    Welcome ABC!