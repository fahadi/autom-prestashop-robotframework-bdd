# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
    Set Test Variable    ${url_home}    http://localhost:8080
	${__TEST_308_SETUP}	Get Variable Value	${TEST 308 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	Open Application Local Tests
	Run Keyword If	$__TEST_308_SETUP is not None	${__TEST_308_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_308_TEARDOWN}	Get Variable Value	${TEST 308 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_308_TEARDOWN is not None	${__TEST_308_TEARDOWN}

*** Test Cases ***
Navigate to one product
	${docstring_1} =	Set Variable	Le meilleur reste à venir ! Faites parler vos murs avec cette affiche encadrée chargée d'optimisme sera du plus bel effet dans un bureau ou un open-space. Cadre en bois peint avec passe-partout integré pour un effet de profondeur.

	[Setup]	Test Setup

	When I navigate to category "art"
	And I navigate to product "Affiche encadrée The best is yet to come"
	Then The product description is "${docstring_1}"

	[Teardown]	Test Teardown