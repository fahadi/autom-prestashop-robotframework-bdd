# Automation priority: null
# Test case importance: High
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
    Set Test Variable    ${url_home}    http://localhost:8080
	${__TEST_302_SETUP}	Get Variable Value	${TEST 302 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	Open Application Local Tests
	Run Keyword If	$__TEST_302_SETUP is not None	${__TEST_302_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_302_TEARDOWN}	Get Variable Value	${TEST 302 TEARDOWN}
	Run Keyword If	$__TEST_302_TEARDOWN is not None	${__TEST_302_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
KOtests_001
	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noel" password "police" email "alice@bob.com" birthDate "01/01/1970" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGpdr "yes" and submit
	And I sign out
	And I sign in with email "alice@bob.com" and password "police"
	Then My personal informations are gender "F" firstName "Bob" lastName "Alice" email "bob@alice.com" birthDate "02/02/1981"

	[Teardown]	Test Teardown