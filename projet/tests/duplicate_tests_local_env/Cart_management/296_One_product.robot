# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
    Set Test Variable    ${url_home}    http://localhost:8080
	${__TEST_296_SETUP}	Get Variable Value	${TEST 296 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	Open Application Local Tests
	Run Keyword If	$__TEST_296_SETUP is not None	${__TEST_296_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_296_TEARDOWN}	Get Variable Value	${TEST 296 TEARDOWN}
	Run Keyword If	$__TEST_296_TEARDOWN is not None	${__TEST_296_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
One product
	${row_1_1} =	Create List	Product	Number	Dimension
	${row_1_2} =	Create List	Affiche encadrée The best is yet to come	1	40x60cm
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}

	[Setup]	Test Setup

	Given I am logged in
	When I navigate to category "art"
	And I navigate to product "Affiche encadrée The best is yet to come"
	And I add to cart
	Then The cart contains "${datatable_1}"

	[Teardown]	Test Teardown