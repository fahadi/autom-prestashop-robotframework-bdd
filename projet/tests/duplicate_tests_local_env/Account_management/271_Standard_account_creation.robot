# Automation priority: 42
# Test case importance: High
*** Settings ***
Documentation    This test 
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Variables ***
${gender}    F
${first}    Alice
${last}    Bob
${password}    Pass1234
${mail}    alice@bob.fr
${birth}    10/08/1998
${offers}    yes
${privacy}    yes
${news}	yes
${gpdr}    yes

*** Keywords ***
Test Setup
    Set Test Variable    ${url_home}    http://localhost:8080
	${__TEST_271_SETUP}	Get Variable Value	${TEST 271 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	Open Application Local Tests
	Run Keyword If	$__TEST_271_SETUP is not None	${__TEST_271_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_271_TEARDOWN}	Get Variable Value	${TEST 271 TEARDOWN}
	Run Keyword If	$__TEST_271_TEARDOWN is not None	${__TEST_271_TEARDOWN}
    Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	
*** Test Cases ***
Standard account creation

	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender ${gender} firstName ${first} lastName ${last} password ${password} email ${mail} birthDate ${birth} acceptPartnerOffers ${offers} acceptPrivacyPolicy ${privacy} acceptNewsletter ${news} acceptGpdr ${gpdr} and submit
	And I sign out
	And I sign in with email ${mail} and password ${password}
	Then My personal informations are gender ${gender} firstName ${first} lastName ${last} email ${mail} birthDate ${birth}

	[Teardown]	Test Teardown