# Automation priority: null
# Test case importance: High
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
    Set Test Variable    ${url_home}    http://localhost:8080
	${__TEST_294_SETUP}	Get Variable Value	${TEST 294 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	Open Application Local Tests
	Run Keyword If	$__TEST_294_SETUP is not None	${__TEST_294_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_294_TEARDOWN}	Get Variable Value	${TEST 294 TEARDOWN}
	Run Keyword If	$__TEST_294_TEARDOWN is not None	${__TEST_294_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Cannot log with incorrect password
	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noel" password "police" email "alice@noel.com" birthDate "01/01/1970" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGpdr "yes" and submit
	And I sign out
	And I sign in with email "alice@noel.com" and password "poluce"
	Then The error message "Échec d'authentification" is displayed

	[Teardown]	Test Teardown