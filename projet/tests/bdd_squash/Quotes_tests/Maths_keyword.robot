*** Settings ***
Library    Collections


*** Keywords ***
"${n1}" + "${n2}" = "${n3}"
    ${sum}=    Evaluate    ${n1}+${n2}
    ${result}=    Evaluate    ${n3}
    Should Be Equal    ${sum}    ${result}

The operations are ok "${datatable_1}"
    ${datas}=    Create ListOfDictionary From Datatable ${datatable_1}
    FOR    ${data}    IN    @{datas}
        Log    ${data}
        "${data}[num1]" + "${data}[num2]" = "${data}[result]"
    END

Create ListOfDictionary From Datatable ${datatable}
    #number of element in list datable
    ${datatable_size} =    Get Length    ${datatable}

    ${final_list} =    Create List
    #Iteration on all elements but the first one (header = future keys)
    FOR    ${i}    IN RANGE    1    ${datatable_size}    1
        #number of attributes:
        ${nb_items} =    Get Length    ${datatable}[${i}]

        ${dict} =    Create Dictionary
        #iteration on each attribute (future values)
        FOR    ${j}    IN RANGE    0    ${nb_items}    1
            #creation of dictionary : key=value
            Set To Dictionary    ${dict}    ${datatable}[0][${j}]=${datatable}[${i}][${j}]
        END
        Append To List    ${final_list}    ${dict}
    END
    RETURN    ${final_list}