# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_315_SETUP}	Get Variable Value	${TEST 315 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_315_SETUP is not None	${__TEST_315_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_315_TEARDOWN}	Get Variable Value	${TEST 315 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_315_TEARDOWN is not None	${__TEST_315_TEARDOWN}

*** Test Cases ***
Strings2
	${s1} =	Get Test Param	DS_s1
	${s2} =	Get Test Param	DS_s2
	${s3} =	Get Test Param	DS_s3

	[Setup]	Test Setup

	Then "${s1}" + "${s2}" = "${s3}"

	[Teardown]	Test Teardown