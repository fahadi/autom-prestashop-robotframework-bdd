*** Settings ***
Library    Collections


*** Keywords ***
"${str1}" + "${str2}" = "${str3}"
    Log To Console    --str1:${str1}
    Log To Console    --str2:${str2}
    Log To Console    --str3:${str3}
    ${concat}=    Set Variable    ${str1}${str2}
    Should Be Equal    ${concat}    ${str3}

The concat are ok "${datatable_1}"
    ${datas}=    Create ListOfDictionary From Datatable ${datatable_1}
    FOR    ${data}    IN    @{datas}
        "${data}[s1]" + "${data}[s2]" = "${data}[s3]"
    END

Create ListOfDictionary From Datatable ${datatable}
    #number of element in list datable
    ${datatable_size} =    Get Length    ${datatable}

    ${final_list} =    Create List
    #Iteration on all elements but the first one (header = future keys)
    FOR    ${i}    IN RANGE    1    ${datatable_size}    1
        #number of attributes:
        ${nb_items} =    Get Length    ${datatable}[${i}]

        ${dict} =    Create Dictionary
        #iteration on each attribute (future values)
        FOR    ${j}    IN RANGE    0    ${nb_items}    1
            #creation of dictionary : key=value
            Set To Dictionary    ${dict}    ${datatable}[0][${j}]=${datatable}[${i}][${j}]
        END
        Append To List    ${final_list}    ${dict}
    END
    RETURN    ${final_list}