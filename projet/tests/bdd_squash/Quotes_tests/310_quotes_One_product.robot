# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_310_SETUP}	Get Variable Value	${TEST 310 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_310_SETUP is not None	${__TEST_310_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_310_TEARDOWN}	Get Variable Value	${TEST 310 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_310_TEARDOWN is not None	${__TEST_310_TEARDOWN}

*** Test Cases ***
quotes_One product
	${row_1_1} =	Create List	Product	Number	Dimension
	${row_1_2} =	Create List	Affiche encadrée The best is yet to come	1	40x60cm
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}

	[Setup]	Test Setup

	Given I am logged in
	When I navigate to category "art"
	And I navigate to product "Affiche encadrée The best is yet to come"
	And I add to cart
	And I go to the Cart page
	Then The cart contains "${datatable_1}"

	[Teardown]	Test Teardown