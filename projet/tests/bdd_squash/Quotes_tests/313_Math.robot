# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_313_SETUP}	Get Variable Value	${TEST 313 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_313_SETUP is not None	${__TEST_313_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_313_TEARDOWN}	Get Variable Value	${TEST 313 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_313_TEARDOWN is not None	${__TEST_313_TEARDOWN}

*** Test Cases ***
Math
	${n1} =	Get Test Param	DS_n1
	${n2} =	Get Test Param	DS_n2
	${n3} =	Get Test Param	DS_n3

	${row_1_1} =	Create List	num1	num2	result
	${row_1_2} =	Create List	1	4	5
	${row_1_3} =	Create List	1.58	4	5.58
	${row_1_4} =	Create List	1.74	4.01	5.75
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}	${row_1_3}	${row_1_4}

	[Setup]	Test Setup

	Then "1" + "2.35" = "3.35"
	Then "3" + "1.53" = "4.53"
	Then "${n1}" + "${n2}" = "${n3}"
	Then The operations are ok "${datatable_1}"

	[Teardown]	Test Teardown