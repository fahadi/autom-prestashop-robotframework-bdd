# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_311_SETUP}	Get Variable Value	${TEST 311 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_311_SETUP is not None	${__TEST_311_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_311_TEARDOWN}	Get Variable Value	${TEST 311 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_311_TEARDOWN is not None	${__TEST_311_TEARDOWN}

*** Test Cases ***
quotes_Dataset_One product
	${category} =	Get Test Param	DS_category
	${product} =	Get Test Param	DS_product

	${row_1_1} =	Create List	Product	Number	Dimension
	${row_1_2} =	Create List	<product>	<number>	<dimension>
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}

	[Setup]	Test Setup

	Given I am logged in
	When I navigate to category ${category}
	And I navigate to product ${product}
	And I add to cart
	And I go to the Cart page
	Then The cart contains "${datatable_1}"

	[Teardown]	Test Teardown