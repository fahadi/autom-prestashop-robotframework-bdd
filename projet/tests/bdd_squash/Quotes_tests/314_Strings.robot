# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_314_SETUP}	Get Variable Value	${TEST 314 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_314_SETUP is not None	${__TEST_314_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_314_TEARDOWN}	Get Variable Value	${TEST 314 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_314_TEARDOWN is not None	${__TEST_314_TEARDOWN}

*** Test Cases ***
Strings
	${s1} =	Get Test Param	DS_s1
	${s2} =	Get Test Param	DS_s2
	${s3} =	Get Test Param	DS_s3

	${row_1_1} =	Create List	s1	s2	s3
	${row_1_2} =	Create List	abc	"def"	abc"def"
	${row_1_3} =	Create List	ca"b	"uu"	ca"b"uu"
	${row_1_4} =	Create List	$xyz	zyx$	$xyzzyx$
	${row_1_5} =	Create List	\$tutu	tu\$tu	\$tututu\$tu
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}	${row_1_3}	${row_1_4}	${row_1_5}

	[Setup]	Test Setup

	Then "5$" + "a" = "5$a"
	Then "$data" + "b" = "$datab"
	Then "titi" + "tutu" = "tititutu"
	Then "${s1}" + "${s2}" = "${s3}"
	Then The concat are ok "${datatable_1}"

	[Teardown]	Test Teardown
