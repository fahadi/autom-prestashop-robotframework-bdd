# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_297_SETUP}	Get Variable Value	${TEST 297 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_297_SETUP is not None	${__TEST_297_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_297_TEARDOWN}	Get Variable Value	${TEST 297 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_297_TEARDOWN is not None	${__TEST_297_TEARDOWN}

*** Test Cases ***
Add two products in the cart
	${row_1_1} =	Create List	Product	Number	Dimension
	${row_1_2} =	Create List	Affiche encadrée The best is yet to come	1	40x60cm
	${row_1_3} =	Create List	Illustration vectorielle Renard	1
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}	${row_1_3}

	[Setup]	Test Setup

	Given I am logged in
	When I navigate to category "art"
	And I navigate to product "Affiche encadrée The best is yet to come"
	And I add to cart
	And I navigate to category "art"
	And I navigate to product "Illustration vectorielle Renard"
	And I add to cart
	Then the cart contains "${datatable_1}"

	[Teardown]	Test Teardown